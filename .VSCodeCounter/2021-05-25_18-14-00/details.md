# Details

Date : 2021-05-25 18:14:00

Directory /Users/munkh-orgil/Desktop/renewal

Total : 42 files,  5933 codes, 21 comments, 241 blanks, all 6195 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [Artists/amc.html](/Artists/amc.html) | HTML | 137 | 0 | 8 | 145 |
| [Artists/bold.html](/Artists/bold.html) | HTML | 137 | 1 | 9 | 147 |
| [Artists/choijoo.html](/Artists/choijoo.html) | HTML | 137 | 1 | 9 | 147 |
| [Artists/ginjin.html](/Artists/ginjin.html) | HTML | 137 | 1 | 9 | 147 |
| [Artists/nene.html](/Artists/nene.html) | HTML | 137 | 1 | 9 | 147 |
| [Artists/nisvanis.html](/Artists/nisvanis.html) | HTML | 137 | 1 | 9 | 147 |
| [Artists/rokitbay.html](/Artists/rokitbay.html) | HTML | 137 | 1 | 9 | 147 |
| [Artists/sash.html](/Artists/sash.html) | HTML | 137 | 1 | 9 | 147 |
| [Artists/thunder.html](/Artists/thunder.html) | HTML | 137 | 1 | 10 | 148 |
| [Artists/vandebo.html](/Artists/vandebo.html) | HTML | 137 | 1 | 9 | 147 |
| [CSS/aboutus.css](/CSS/aboutus.css) | CSS | 178 | 0 | 0 | 178 |
| [CSS/artists-main.css](/CSS/artists-main.css) | CSS | 171 | 1 | 6 | 178 |
| [CSS/index-main.css](/CSS/index-main.css) | CSS | 18 | 0 | 0 | 18 |
| [CSS/index-new.css](/CSS/index-new.css) | CSS | 117 | 0 | 2 | 119 |
| [CSS/index-playlists.css](/CSS/index-playlists.css) | CSS | 96 | 0 | 0 | 96 |
| [CSS/index-topArtists.css](/CSS/index-topArtists.css) | CSS | 66 | 0 | 2 | 68 |
| [CSS/mix-main.css](/CSS/mix-main.css) | CSS | 114 | 1 | 0 | 115 |
| [CSS/mix.css](/CSS/mix.css) | CSS | 171 | 1 | 6 | 178 |
| [CSS/modal-player.css](/CSS/modal-player.css) | CSS | 193 | 0 | 3 | 196 |
| [CSS/new-main.css](/CSS/new-main.css) | CSS | 124 | 0 | 2 | 126 |
| [CSS/search-bar.css](/CSS/search-bar.css) | CSS | 54 | 3 | 1 | 58 |
| [CSS/song-search.css](/CSS/song-search.css) | CSS | 274 | 1 | 4 | 279 |
| [CSS/style-menu.css](/CSS/style-menu.css) | CSS | 100 | 3 | 0 | 103 |
| [CSS/user.css](/CSS/user.css) | CSS | 163 | 1 | 2 | 166 |
| [JS/aritsts.js](/JS/aritsts.js) | JavaScript | 200 | 0 | 7 | 207 |
| [JS/data.js](/JS/data.js) | JavaScript | 309 | 0 | 6 | 315 |
| [JS/index-songs.js](/JS/index-songs.js) | JavaScript | 185 | 0 | 3 | 188 |
| [JS/index-top.js](/JS/index-top.js) | JavaScript | 18 | 0 | 1 | 19 |
| [JS/mix.js](/JS/mix.js) | JavaScript | 257 | 0 | 7 | 264 |
| [JS/new.js](/JS/new.js) | JavaScript | 185 | 0 | 4 | 189 |
| [JS/search-artists.js](/JS/search-artists.js) | JavaScript | 31 | 0 | 1 | 32 |
| [JS/search.js](/JS/search.js) | JavaScript | 248 | 0 | 9 | 257 |
| [JS/user.js](/JS/user.js) | JavaScript | 53 | 0 | 4 | 57 |
| [Mix/E-Mix.html](/Mix/E-Mix.html) | HTML | 135 | 0 | 9 | 144 |
| [Mix/N-Mix.html](/Mix/N-Mix.html) | HTML | 135 | 0 | 10 | 145 |
| [Mix/O-Mix.html](/Mix/O-Mix.html) | HTML | 135 | 0 | 9 | 144 |
| [aboutus.html](/aboutus.html) | HTML | 197 | 0 | 9 | 206 |
| [home.html](/home.html) | HTML | 184 | 0 | 12 | 196 |
| [index.html](/index.html) | HTML | 59 | 0 | 1 | 60 |
| [mix.html](/mix.html) | HTML | 106 | 0 | 8 | 114 |
| [new.html](/new.html) | HTML | 135 | 0 | 9 | 144 |
| [search.html](/search.html) | HTML | 152 | 1 | 14 | 167 |

[summary](results.md)