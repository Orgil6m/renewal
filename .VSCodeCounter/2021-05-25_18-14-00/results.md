# Summary

Date : 2021-05-25 18:14:00

Directory /Users/munkh-orgil/Desktop/renewal

Total : 42 files,  5933 codes, 21 comments, 241 blanks, all 6195 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| HTML | 19 | 2,608 | 10 | 171 | 2,789 |
| CSS | 14 | 1,839 | 11 | 28 | 1,878 |
| JavaScript | 9 | 1,486 | 0 | 42 | 1,528 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 42 | 5,933 | 21 | 241 | 6,195 |
| Artists | 10 | 1,370 | 9 | 90 | 1,469 |
| CSS | 14 | 1,839 | 11 | 28 | 1,878 |
| JS | 9 | 1,486 | 0 | 42 | 1,528 |
| Mix | 3 | 405 | 0 | 28 | 433 |

[details](details.md)